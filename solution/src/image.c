#include "image.h"
#include "stdlib.h"

void init_image(uint64_t width, uint64_t height, struct image *img){
  img->width = width;
  img->height = height;
  img->data = malloc(sizeof(struct pixel) * width * height);
}

void destruct_image(struct image* image) {
  free(image->data);
}
