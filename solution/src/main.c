#include "bmp.h"
#include "image.h"
#include "rotation_img.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc == 3) {
        FILE *in = fopen(argv[1], "rb");
        FILE *out = fopen(argv[2], "wb");
        
        if (in != NULL && out != NULL) { 
            struct image img = {0};

            if (from_bmp(in, &img) != READ_OK) {
                fprintf(stderr,"Error: Invalid input/n");
                return 3;
            }

            struct image rotated_img = rotate_90(&img);

            if (to_bmp(out, &rotated_img) != WRITE_OK) {
                fprintf(stderr,"Error: Invalid out/n");
                return 4;
            }

            destruct_image(&img);
            destruct_image(&rotated_img);
            fclose(in);
            fclose(out);
        }else {
            fprintf(stderr,"Error: Files don't exist/n");
            return 2;
        }
    }
    

    
    return 0;
}
