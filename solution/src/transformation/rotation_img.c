#include "rotation_img.h"
#include "image.h"
#include <inttypes.h> 
#include <stdio.h>
#include <stdlib.h>

static uint64_t get_cord(uint64_t i, uint64_t j, uint64_t width) {
    return (i * width + j);
}

static uint64_t set_cord(uint64_t i, uint64_t j, uint64_t height) {
    return (j * height + (height - 1 -i));
}

struct image rotate_90( struct image const* source ) {

    struct pixel *pixels = malloc(sizeof(struct pixel) * source->width * source->height);
    struct image final_image = {.width = source->height, .height = source->width, .data = pixels};

    for (uint64_t i = 0; i < source->height; i++) {
        for (uint64_t j = 0; j < source->width; j++) {
            final_image.data[ set_cord(i, j, source->height) ] = source->data[ get_cord(i, j, source->width) ];
        }
    }
    return final_image;
}
