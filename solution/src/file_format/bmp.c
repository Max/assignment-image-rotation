#include "bmp.h"
#include "image.h"
#include <stdlib.h>


#define BM_IDENTIFIER 0x4D42
#define PLANES_NUM 1
#define BIPLANES 1
#define INFO_HEADER_SIZE 40
#define TYPE 19778
#define BITCOUNT 24
#define BISIZE 40
#define BOFFBITS 54

struct __attribute__((packed)) bmp_header 
{
    uint16_t  bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static uint32_t get_padding(uint32_t width) {
    return (4 - width * 3 % 4) % 4;
}

static uint32_t get_image_size(uint32_t width, uint32_t height) {
    return (3 * width + get_padding(width)) * height;    
}

static struct bmp_header new_bmp_header(uint32_t biSizeImage, uint32_t width, uint32_t height) {
    struct bmp_header header = {0};
    header.bfType = TYPE;
    header.bfileSize = sizeof(struct bmp_header) + biSizeImage;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BISIZE;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = BIPLANES;
    header.biBitCount = BITCOUNT;
    header.biSizeImage = biSizeImage;
    return header;
}

enum read_status read_bmp_header(FILE *in, struct bmp_header* header ) {
    fread(header, sizeof(struct bmp_header), 1, in);
    if (ferror(in) == 0 ) {
        return READ_OK;
    }else {
        return READ_INVALID_HEADER;
    }
}

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header;

    if (read_bmp_header(in, &header) != READ_OK) return READ_INVALID_HEADER;
    fseek(in, header.bOffBits, SEEK_SET);

    uint32_t padding = get_padding( header.biWidth );
    init_image(header.biWidth, header.biHeight, img);

    for (uint32_t i = 0; i < img->height; i++) {
        fread(img->data + i * img->width, sizeof(struct pixel), img->width, in);
        if (ferror(in) != 0) {
            destruct_image(img); 
            return READ_DATA_ERROR;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            destruct_image(img);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    uint32_t padding = get_padding(img->width);
    uint32_t size_image = get_image_size(img->width, img->height);

    struct bmp_header header = new_bmp_header(size_image, img->width, img->height);

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (ferror(out) != 0) return WRITE_HEADER_ERROR;

    const char trash[] = {0, 0, 0}; 

    for (uint32_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        fwrite(trash, 1, padding, out);
        if (ferror(out) != 0) { 
            return WRITE_DATA_ERROR;
        }
    }
    return WRITE_OK;
}
