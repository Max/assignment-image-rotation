#pragma once

#include <inttypes.h>

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

void init_image(uint64_t width, uint64_t height, struct image *img);

void destruct_image(struct image* image);
